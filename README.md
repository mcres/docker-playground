# docker-hello-world

A simple package for starting with Docker:
* `cpp-hello-world`: outputs a simple hello-world message using C++.
* `docker-hello-world-compose`: the hello-world docker image run with docker-compose.
* `docker-hello-world-volume`: write a simple text file and save in persistent memory using docker volumes.
* `python-hello-world`: outputs a simple hello-world message using Python.
* [TODO] `ros2-depend-on-docker-v1`: expose and link a docker container to the ros2 build system.
* [TODO] `ros2-depend-on-docker-v2`: similar to v1, but in this example the ROS 2 package is also in a Docker container, not locally.
* [TODO] `ros2-listen-to-docker`: the local ROS 2 can listen to ROS 2 data generated in a docker container.
* `ros2-hello-world`: installs a ROS 2 package and writes its name in the console.
* `ros2-two-separate-containers`: launch 2 docker containers, each one containing a talker-listener ROS 2 node.
