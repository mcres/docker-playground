# Step 1: create image from dockerfile, which contains a Python module
docker build -t ros2-expose-img .

# Step 2: run container with --net host so container gets exposed to ros2
docker run --rm --net=host ros2-expose-img 

# Step 3: locally, create a ROS 2 package that imports the Python module living on Docker

# Step 4: build the ROS 2 package

# Step 5: run ROS 2 node and check that import from Docker module is correct
