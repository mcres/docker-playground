import rclpy
from docker_data_library import data_to_publish
from rclpy.node import Node
from std_msgs.msg import String

class MyPublisher(Node):

    def __init__(self):
        super().__init__('custom_topic')
        self.publisher_ = self.create_publisher(String, 'my_custom_topic', 10)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.publish_message)

    def publish_message(self):
        msg = String()

        # data to publish is defined in a Docker-running library (i.e. data_library)
        msg.data = data_to_publish

        self.publisher_.publish(msg)
        self.get_logger().info('Published message: %s' % msg.data)

def main(args=None):
    rclpy.init(args=args)
    publisher = MyPublisher()
    rclpy.spin(publisher)
    publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()