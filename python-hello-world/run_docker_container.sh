# Create the image from the dockerfile
docker build -t python-hello-world .

# Run a container based on the image we have just created
docker run --name first-container python-hello-world
