# Build the docker compose:
# 1 ros2-based docker container publishes
# 1 ros2-based docker container listens
docker-compose up -d

# Actually see the ros2-produced logs by listening to the docker compose
docker-compose logs listener
