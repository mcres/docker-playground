# Create the image from the dockerfile
docker build -t hello-world-volume .

# Run a container based on the image we have just created
docker run -v 1234abcd:/tmp/docker-test/ hello-world-volume

# Check that the file has been written in the local host
sudo ls /var/lib/docker/volumes/1234abcd/_data
sudo cat /var/lib/docker/volumes/1234abcd/_data/data.txt
